package Impl;

import Interface.CacheItem;

public class CacheItemImpl implements CacheItem {

  private String key;
  private Object value;

  public CacheItemImpl(String key, Object value) {
    this.key = key;
    this.value = value;
  }

  public String getKey() {
    return key;
  }

  public Object getValue() {
    return value;
  }

  @Override
  public String toString() {
    return
        "key='" + key + '\'' +
            ", value=" + value +
            '}';
  }
}
