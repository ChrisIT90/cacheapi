package Impl;

import Interface.Cache;
import Interface.CacheItem;
import Interface.CacheView;
import java.util.LinkedHashMap;
import java.util.Map;

public class CacheAPI implements Cache, CacheView {

  private final Map<String, CacheItemImpl> cache;

  public CacheAPI(final int maxEntries) {
    this.cache = new LinkedHashMap<String, CacheItemImpl>(maxEntries, 0.75F, true) {
      @Override
      protected boolean removeEldestEntry(Map.Entry<String, CacheItemImpl> eldest) {
        return size() > maxEntries;
      }
    };
  }

  public CacheItem cacheItem(Object item, String key) {
    CacheItemImpl cacheItemImpl = new CacheItemImpl(key, item);
    synchronized (cache) {
      cache.put(key, cacheItemImpl);
      return cacheItemImpl;
    }
  }

  public void invalidateCache() {
    cache.clear();
  }

  public CacheView getView() {

    return this;
  }

  public int size() {
    return cache.size();
  }

  public CacheItem getItem(int index) {
    return getItem((String) cache.keySet().toArray()[index]);
  }

  public CacheItem getItem(String key) {
    synchronized (cache) {
      CacheItemImpl item = cache.get(key);
      if (item == null) {
        cacheItem(key, key);
      }
      return item;
    }

  }

  @Override
  public String toString() {
    return
        "cache=" + cache +
            '}';
  }
}

