package Interface;

public interface CacheItem {

  String getKey(); // returns object key

  Object getValue(); //returns the object stred in tha cache

}
