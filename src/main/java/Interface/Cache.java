package Interface;

public interface Cache {

  CacheItem cacheItem(Object item, String key); // save object to cache

  void invalidateCache();// clean cache

  CacheView getView(); // return cache

}
