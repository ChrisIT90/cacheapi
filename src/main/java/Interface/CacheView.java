package Interface;

public interface CacheView {

  int size();// returns the size of the cache

  CacheItem getItem(int index);// returns the object with the specified index

  CacheItem getItem(String key);// returns the object with the specified key

}
