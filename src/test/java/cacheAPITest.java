import Impl.CacheAPI;
import org.junit.Assert;
import org.junit.Test;

public class cacheAPITest {

  @Test
  public void checkSizeOfCache() {
    CacheAPI cacheAPI = new CacheAPI(3);
    cacheAPI.cacheItem(1, "A");
    cacheAPI.cacheItem(2, "B");
    cacheAPI.cacheItem(3, "C");
    cacheAPI.cacheItem(4, "D");
    Assert.assertEquals(3, cacheAPI.size());
  }

  @Test
  public void getItemByKey() {
    CacheAPI cacheAPI = new CacheAPI(3);
    cacheAPI.cacheItem(1, "A");
    cacheAPI.cacheItem(2, "B");
    cacheAPI.cacheItem(3, "C");

    Assert.assertEquals(cacheAPI.cacheItem(2, "B"), cacheAPI.getItem(2));
  }

  @Test
  public void getItemByVale() {
    CacheAPI cacheAPI = new CacheAPI(3);
    cacheAPI.cacheItem(1, "A");
    cacheAPI.cacheItem(2, "B");
    cacheAPI.cacheItem(3, "C");

    Assert.assertEquals(cacheAPI.cacheItem(2, "B"), cacheAPI.getItem("B"));

  }


}