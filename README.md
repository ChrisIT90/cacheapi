# Smart Cache

This is Cache that remembers x recently added objects.

# Dependencies
Make sure you have latest Java 8, Maven.

# Running API
Please use the following steps for running API locally on your machine:

1.Clone this repository to your local machine. 
2.Navigate to repository in console 
3.Run command mvn test

# Test Coverage
1.Check size of cache
2.Get Item by key
3.Get Item by value

The main reason for this project is writing a clever cache that will remember the recently added objects.
This is because the application does not slow down because of repeated requests that return identical results for the same set of parameters.
